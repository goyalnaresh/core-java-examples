package com.example;

import org.junit.Assert;
import org.junit.Test;

public class SumCalculatorTest {

	@Test
	public void testSum() {
		SumCalculator calculator = new SumCalculator();
		Assert.assertEquals(5, calculator.sum(2, 3));
	}

}
